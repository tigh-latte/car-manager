﻿namespace CarManager {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.grpCharacteristics = new System.Windows.Forms.GroupBox();
            this.lbPrice = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtMilage = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.lbMilage = new System.Windows.Forms.Label();
            this.lbYear = new System.Windows.Forms.Label();
            this.lbModel = new System.Windows.Forms.Label();
            this.lbBrand = new System.Windows.Forms.Label();
            this.grpBodyType = new System.Windows.Forms.GroupBox();
            this.rbCoupe = new System.Windows.Forms.RadioButton();
            this.rbSUV = new System.Windows.Forms.RadioButton();
            this.rbConvertible = new System.Windows.Forms.RadioButton();
            this.rbMPV = new System.Windows.Forms.RadioButton();
            this.rbEstate = new System.Windows.Forms.RadioButton();
            this.rbSaloon = new System.Windows.Forms.RadioButton();
            this.rbHatchback = new System.Windows.Forms.RadioButton();
            this.grpGearbox = new System.Windows.Forms.GroupBox();
            this.rbAutomatic = new System.Windows.Forms.RadioButton();
            this.rbManual = new System.Windows.Forms.RadioButton();
            this.grpInformation = new System.Windows.Forms.GroupBox();
            this.txtAdd = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClearInformation = new System.Windows.Forms.Button();
            this.lbCreated = new System.Windows.Forms.Label();
            this.btnEditCar = new System.Windows.Forms.Button();
            this.btnSaveCar = new System.Windows.Forms.Button();
            this.lbLastEdited = new System.Windows.Forms.Label();
            this.btnPreviousCar = new System.Windows.Forms.Button();
            this.btnNextCar = new System.Windows.Forms.Button();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFileNewCar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFileNewDatabase = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmEditDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmView = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSortBy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSortByBrand = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSortByYear = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSortByPrice = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSelect = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSelectBrand = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmViewSelectYear = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelpHowToUse = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.pbImageOfCar = new System.Windows.Forms.PictureBox();
            this.listInfomation = new System.Windows.Forms.ListBox();
            this.grpCharacteristics.SuspendLayout();
            this.grpBodyType.SuspendLayout();
            this.grpGearbox.SuspendLayout();
            this.grpInformation.SuspendLayout();
            this.msMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImageOfCar)).BeginInit();
            this.SuspendLayout();
            // 
            // grpCharacteristics
            // 
            this.grpCharacteristics.Controls.Add(this.lbPrice);
            this.grpCharacteristics.Controls.Add(this.txtPrice);
            this.grpCharacteristics.Controls.Add(this.txtMilage);
            this.grpCharacteristics.Controls.Add(this.txtYear);
            this.grpCharacteristics.Controls.Add(this.txtModel);
            this.grpCharacteristics.Controls.Add(this.txtBrand);
            this.grpCharacteristics.Controls.Add(this.lbMilage);
            this.grpCharacteristics.Controls.Add(this.lbYear);
            this.grpCharacteristics.Controls.Add(this.lbModel);
            this.grpCharacteristics.Controls.Add(this.lbBrand);
            this.grpCharacteristics.Location = new System.Drawing.Point(12, 27);
            this.grpCharacteristics.Name = "grpCharacteristics";
            this.grpCharacteristics.Size = new System.Drawing.Size(283, 187);
            this.grpCharacteristics.TabIndex = 0;
            this.grpCharacteristics.TabStop = false;
            this.grpCharacteristics.Text = "Characteristics";
            // 
            // lbPrice
            // 
            this.lbPrice.AutoSize = true;
            this.lbPrice.Location = new System.Drawing.Point(5, 142);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(37, 13);
            this.lbPrice.TabIndex = 9;
            this.lbPrice.Text = "Price: ";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(53, 139);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 20);
            this.txtPrice.TabIndex = 4;
            // 
            // txtMilage
            // 
            this.txtMilage.Location = new System.Drawing.Point(53, 110);
            this.txtMilage.Name = "txtMilage";
            this.txtMilage.Size = new System.Drawing.Size(100, 20);
            this.txtMilage.TabIndex = 3;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(53, 81);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(100, 20);
            this.txtYear.TabIndex = 2;
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(53, 51);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(100, 20);
            this.txtModel.TabIndex = 1;
            // 
            // txtBrand
            // 
            this.txtBrand.Location = new System.Drawing.Point(53, 21);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(100, 20);
            this.txtBrand.TabIndex = 0;
            // 
            // lbMilage
            // 
            this.lbMilage.AutoSize = true;
            this.lbMilage.Location = new System.Drawing.Point(6, 113);
            this.lbMilage.Name = "lbMilage";
            this.lbMilage.Size = new System.Drawing.Size(44, 13);
            this.lbMilage.TabIndex = 3;
            this.lbMilage.Text = "Milage: ";
            // 
            // lbYear
            // 
            this.lbYear.AutoSize = true;
            this.lbYear.Location = new System.Drawing.Point(6, 84);
            this.lbYear.Name = "lbYear";
            this.lbYear.Size = new System.Drawing.Size(35, 13);
            this.lbYear.TabIndex = 2;
            this.lbYear.Text = "Year: ";
            // 
            // lbModel
            // 
            this.lbModel.AutoSize = true;
            this.lbModel.Location = new System.Drawing.Point(5, 54);
            this.lbModel.Name = "lbModel";
            this.lbModel.Size = new System.Drawing.Size(42, 13);
            this.lbModel.TabIndex = 1;
            this.lbModel.Text = "Model: ";
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.Location = new System.Drawing.Point(6, 24);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(41, 13);
            this.lbBrand.TabIndex = 0;
            this.lbBrand.Text = "Brand: ";
            // 
            // grpBodyType
            // 
            this.grpBodyType.Controls.Add(this.rbCoupe);
            this.grpBodyType.Controls.Add(this.rbSUV);
            this.grpBodyType.Controls.Add(this.rbConvertible);
            this.grpBodyType.Controls.Add(this.rbMPV);
            this.grpBodyType.Controls.Add(this.rbEstate);
            this.grpBodyType.Controls.Add(this.rbSaloon);
            this.grpBodyType.Controls.Add(this.rbHatchback);
            this.grpBodyType.Location = new System.Drawing.Point(13, 220);
            this.grpBodyType.Name = "grpBodyType";
            this.grpBodyType.Size = new System.Drawing.Size(282, 91);
            this.grpBodyType.TabIndex = 1;
            this.grpBodyType.TabStop = false;
            this.grpBodyType.Text = "Body Type";
            // 
            // rbCoupe
            // 
            this.rbCoupe.AutoSize = true;
            this.rbCoupe.Location = new System.Drawing.Point(191, 41);
            this.rbCoupe.Name = "rbCoupe";
            this.rbCoupe.Size = new System.Drawing.Size(56, 17);
            this.rbCoupe.TabIndex = 5;
            this.rbCoupe.TabStop = true;
            this.rbCoupe.Text = "Coupe";
            this.rbCoupe.UseVisualStyleBackColor = true;
            // 
            // rbSUV
            // 
            this.rbSUV.AutoSize = true;
            this.rbSUV.Location = new System.Drawing.Point(191, 20);
            this.rbSUV.Name = "rbSUV";
            this.rbSUV.Size = new System.Drawing.Size(47, 17);
            this.rbSUV.TabIndex = 2;
            this.rbSUV.TabStop = true;
            this.rbSUV.Text = "SUV";
            this.rbSUV.UseVisualStyleBackColor = true;
            // 
            // rbConvertible
            // 
            this.rbConvertible.AutoSize = true;
            this.rbConvertible.Location = new System.Drawing.Point(100, 42);
            this.rbConvertible.Name = "rbConvertible";
            this.rbConvertible.Size = new System.Drawing.Size(78, 17);
            this.rbConvertible.TabIndex = 4;
            this.rbConvertible.TabStop = true;
            this.rbConvertible.Text = "Convertible";
            this.rbConvertible.UseVisualStyleBackColor = true;
            // 
            // rbMPV
            // 
            this.rbMPV.AutoSize = true;
            this.rbMPV.Location = new System.Drawing.Point(100, 20);
            this.rbMPV.Name = "rbMPV";
            this.rbMPV.Size = new System.Drawing.Size(48, 17);
            this.rbMPV.TabIndex = 1;
            this.rbMPV.TabStop = true;
            this.rbMPV.Text = "MPV";
            this.rbMPV.UseVisualStyleBackColor = true;
            // 
            // rbEstate
            // 
            this.rbEstate.AutoSize = true;
            this.rbEstate.Location = new System.Drawing.Point(8, 64);
            this.rbEstate.Name = "rbEstate";
            this.rbEstate.Size = new System.Drawing.Size(55, 17);
            this.rbEstate.TabIndex = 6;
            this.rbEstate.TabStop = true;
            this.rbEstate.Text = "Estate";
            this.rbEstate.UseVisualStyleBackColor = true;
            // 
            // rbSaloon
            // 
            this.rbSaloon.AutoSize = true;
            this.rbSaloon.Location = new System.Drawing.Point(8, 42);
            this.rbSaloon.Name = "rbSaloon";
            this.rbSaloon.Size = new System.Drawing.Size(58, 17);
            this.rbSaloon.TabIndex = 3;
            this.rbSaloon.TabStop = true;
            this.rbSaloon.Text = "Saloon";
            this.rbSaloon.UseVisualStyleBackColor = true;
            // 
            // rbHatchback
            // 
            this.rbHatchback.AutoSize = true;
            this.rbHatchback.Location = new System.Drawing.Point(8, 20);
            this.rbHatchback.Name = "rbHatchback";
            this.rbHatchback.Size = new System.Drawing.Size(78, 17);
            this.rbHatchback.TabIndex = 0;
            this.rbHatchback.TabStop = true;
            this.rbHatchback.Text = "Hatchback";
            this.rbHatchback.UseVisualStyleBackColor = true;
            // 
            // grpGearbox
            // 
            this.grpGearbox.Controls.Add(this.rbAutomatic);
            this.grpGearbox.Controls.Add(this.rbManual);
            this.grpGearbox.Location = new System.Drawing.Point(13, 317);
            this.grpGearbox.Name = "grpGearbox";
            this.grpGearbox.Size = new System.Drawing.Size(282, 56);
            this.grpGearbox.TabIndex = 2;
            this.grpGearbox.TabStop = false;
            this.grpGearbox.Text = "Gearbox";
            // 
            // rbAutomatic
            // 
            this.rbAutomatic.AutoSize = true;
            this.rbAutomatic.Location = new System.Drawing.Point(191, 24);
            this.rbAutomatic.Name = "rbAutomatic";
            this.rbAutomatic.Size = new System.Drawing.Size(72, 17);
            this.rbAutomatic.TabIndex = 1;
            this.rbAutomatic.TabStop = true;
            this.rbAutomatic.Text = "Automatic";
            this.rbAutomatic.UseVisualStyleBackColor = true;
            // 
            // rbManual
            // 
            this.rbManual.AutoSize = true;
            this.rbManual.Location = new System.Drawing.Point(8, 24);
            this.rbManual.Name = "rbManual";
            this.rbManual.Size = new System.Drawing.Size(60, 17);
            this.rbManual.TabIndex = 0;
            this.rbManual.TabStop = true;
            this.rbManual.Text = "Manual";
            this.rbManual.UseVisualStyleBackColor = true;
            // 
            // grpInformation
            // 
            this.grpInformation.Controls.Add(this.txtAdd);
            this.grpInformation.Controls.Add(this.btnAdd);
            this.grpInformation.Controls.Add(this.btnClearInformation);
            this.grpInformation.Controls.Add(this.listInfomation);
            this.grpInformation.Location = new System.Drawing.Point(301, 32);
            this.grpInformation.Name = "grpInformation";
            this.grpInformation.Size = new System.Drawing.Size(266, 261);
            this.grpInformation.TabIndex = 3;
            this.grpInformation.TabStop = false;
            this.grpInformation.Text = "Information";
            // 
            // txtAdd
            // 
            this.txtAdd.Location = new System.Drawing.Point(87, 229);
            this.txtAdd.Name = "txtAdd";
            this.txtAdd.Size = new System.Drawing.Size(173, 20);
            this.txtAdd.TabIndex = 2;
            this.txtAdd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAdd_KeyDown);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(6, 227);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClearInformation
            // 
            this.btnClearInformation.Location = new System.Drawing.Point(6, 198);
            this.btnClearInformation.Name = "btnClearInformation";
            this.btnClearInformation.Size = new System.Drawing.Size(254, 23);
            this.btnClearInformation.TabIndex = 0;
            this.btnClearInformation.Text = "Clear Information";
            this.btnClearInformation.UseVisualStyleBackColor = true;
            this.btnClearInformation.Click += new System.EventHandler(this.btnClearInformation_Click);
            // 
            // lbCreated
            // 
            this.lbCreated.AutoSize = true;
            this.lbCreated.Location = new System.Drawing.Point(13, 397);
            this.lbCreated.Name = "lbCreated";
            this.lbCreated.Size = new System.Drawing.Size(50, 13);
            this.lbCreated.TabIndex = 4;
            this.lbCreated.Text = "Created: ";
            // 
            // btnEditCar
            // 
            this.btnEditCar.Location = new System.Drawing.Point(220, 392);
            this.btnEditCar.Name = "btnEditCar";
            this.btnEditCar.Size = new System.Drawing.Size(75, 23);
            this.btnEditCar.TabIndex = 4;
            this.btnEditCar.Text = "Edit Car";
            this.btnEditCar.UseVisualStyleBackColor = true;
            this.btnEditCar.Click += new System.EventHandler(this.btnEditCar_Click);
            // 
            // btnSaveCar
            // 
            this.btnSaveCar.Location = new System.Drawing.Point(220, 434);
            this.btnSaveCar.Name = "btnSaveCar";
            this.btnSaveCar.Size = new System.Drawing.Size(75, 23);
            this.btnSaveCar.TabIndex = 5;
            this.btnSaveCar.Text = "Save Car";
            this.btnSaveCar.UseVisualStyleBackColor = true;
            this.btnSaveCar.Click += new System.EventHandler(this.btnSaveCar_Click);
            // 
            // lbLastEdited
            // 
            this.lbLastEdited.AutoSize = true;
            this.lbLastEdited.Location = new System.Drawing.Point(13, 439);
            this.lbLastEdited.Name = "lbLastEdited";
            this.lbLastEdited.Size = new System.Drawing.Size(54, 13);
            this.lbLastEdited.TabIndex = 7;
            this.lbLastEdited.Text = "Last Edit: ";
            // 
            // btnPreviousCar
            // 
            this.btnPreviousCar.Location = new System.Drawing.Point(12, 468);
            this.btnPreviousCar.Name = "btnPreviousCar";
            this.btnPreviousCar.Size = new System.Drawing.Size(130, 64);
            this.btnPreviousCar.TabIndex = 6;
            this.btnPreviousCar.Text = "<< Previous Car";
            this.btnPreviousCar.UseVisualStyleBackColor = true;
            this.btnPreviousCar.Click += new System.EventHandler(this.btnPreviousCar_Click);
            // 
            // btnNextCar
            // 
            this.btnNextCar.Location = new System.Drawing.Point(165, 468);
            this.btnNextCar.Name = "btnNextCar";
            this.btnNextCar.Size = new System.Drawing.Size(130, 64);
            this.btnNextCar.TabIndex = 7;
            this.btnNextCar.Text = "Next Car >>";
            this.btnNextCar.UseVisualStyleBackColor = true;
            this.btnNextCar.Click += new System.EventHandler(this.btnNextCar_Click);
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFile,
            this.tsmEdit,
            this.tsmView,
            this.tsmHelp});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.msMain.Size = new System.Drawing.Size(578, 24);
            this.msMain.TabIndex = 11;
            this.msMain.Text = "msMain";
            // 
            // tsmFile
            // 
            this.tsmFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFileNew,
            this.tsmFileOpen,
            this.tsmFileSave,
            this.tsmFileSaveAs,
            this.tsmExit});
            this.tsmFile.Name = "tsmFile";
            this.tsmFile.Size = new System.Drawing.Size(37, 20);
            this.tsmFile.Text = "File";
            // 
            // tsmFileNew
            // 
            this.tsmFileNew.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmFileNewCar,
            this.tsmFileNewDatabase});
            this.tsmFileNew.Name = "tsmFileNew";
            this.tsmFileNew.Size = new System.Drawing.Size(218, 22);
            this.tsmFileNew.Text = "New";
            // 
            // tsmFileNewCar
            // 
            this.tsmFileNewCar.Name = "tsmFileNewCar";
            this.tsmFileNewCar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmFileNewCar.Size = new System.Drawing.Size(197, 22);
            this.tsmFileNewCar.Text = "Car";
            this.tsmFileNewCar.Click += new System.EventHandler(this.tsmFileNewCar_Click);
            // 
            // tsmFileNewDatabase
            // 
            this.tsmFileNewDatabase.Name = "tsmFileNewDatabase";
            this.tsmFileNewDatabase.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.N)));
            this.tsmFileNewDatabase.Size = new System.Drawing.Size(197, 22);
            this.tsmFileNewDatabase.Text = "Database";
            this.tsmFileNewDatabase.Click += new System.EventHandler(this.tsmFileNewDatabase_Click);
            // 
            // tsmFileOpen
            // 
            this.tsmFileOpen.Name = "tsmFileOpen";
            this.tsmFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
            this.tsmFileOpen.Size = new System.Drawing.Size(218, 22);
            this.tsmFileOpen.Text = "Open";
            this.tsmFileOpen.Click += new System.EventHandler(this.tsmFileOpen_Click);
            // 
            // tsmFileSave
            // 
            this.tsmFileSave.Enabled = false;
            this.tsmFileSave.Name = "tsmFileSave";
            this.tsmFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.tsmFileSave.Size = new System.Drawing.Size(218, 22);
            this.tsmFileSave.Text = "Save";
            this.tsmFileSave.Click += new System.EventHandler(this.tsmFileSave_Click);
            // 
            // tsmFileSaveAs
            // 
            this.tsmFileSaveAs.Name = "tsmFileSaveAs";
            this.tsmFileSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.tsmFileSaveAs.Size = new System.Drawing.Size(218, 22);
            this.tsmFileSaveAs.Text = "Save As...";
            this.tsmFileSaveAs.Click += new System.EventHandler(this.tsmFileSaveAs_Click);
            // 
            // tsmExit
            // 
            this.tsmExit.Name = "tsmExit";
            this.tsmExit.Size = new System.Drawing.Size(218, 22);
            this.tsmExit.Text = "Exit";
            this.tsmExit.Click += new System.EventHandler(this.tsmExit_Click);
            // 
            // tsmEdit
            // 
            this.tsmEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmEditCut,
            this.tsmEditCopy,
            this.tsmEditPaste,
            this.tsmEditDelete});
            this.tsmEdit.Name = "tsmEdit";
            this.tsmEdit.Size = new System.Drawing.Size(39, 20);
            this.tsmEdit.Text = "Edit";
            // 
            // tsmEditCut
            // 
            this.tsmEditCut.Name = "tsmEditCut";
            this.tsmEditCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.tsmEditCut.Size = new System.Drawing.Size(144, 22);
            this.tsmEditCut.Text = "Cut";
            this.tsmEditCut.Click += new System.EventHandler(this.tsmEditCut_Click);
            // 
            // tsmEditCopy
            // 
            this.tsmEditCopy.Name = "tsmEditCopy";
            this.tsmEditCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.tsmEditCopy.Size = new System.Drawing.Size(144, 22);
            this.tsmEditCopy.Text = "Copy";
            this.tsmEditCopy.Click += new System.EventHandler(this.tsmEditCopy_Click);
            // 
            // tsmEditPaste
            // 
            this.tsmEditPaste.Enabled = false;
            this.tsmEditPaste.Name = "tsmEditPaste";
            this.tsmEditPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.tsmEditPaste.Size = new System.Drawing.Size(144, 22);
            this.tsmEditPaste.Text = "Paste";
            this.tsmEditPaste.Click += new System.EventHandler(this.tsmEditPaste_Click);
            // 
            // tsmEditDelete
            // 
            this.tsmEditDelete.Name = "tsmEditDelete";
            this.tsmEditDelete.Size = new System.Drawing.Size(144, 22);
            this.tsmEditDelete.Text = "Delete";
            this.tsmEditDelete.Click += new System.EventHandler(this.tsmEditDelete_Click);
            // 
            // tsmView
            // 
            this.tsmView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmViewSortBy,
            this.tsmViewSelect});
            this.tsmView.Name = "tsmView";
            this.tsmView.Size = new System.Drawing.Size(44, 20);
            this.tsmView.Text = "View";
            // 
            // tsmViewSortBy
            // 
            this.tsmViewSortBy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmViewSortByBrand,
            this.tsmViewSortByYear,
            this.tsmViewSortByPrice});
            this.tsmViewSortBy.Name = "tsmViewSortBy";
            this.tsmViewSortBy.Size = new System.Drawing.Size(111, 22);
            this.tsmViewSortBy.Text = "Sort By";
            // 
            // tsmViewSortByBrand
            // 
            this.tsmViewSortByBrand.Name = "tsmViewSortByBrand";
            this.tsmViewSortByBrand.Size = new System.Drawing.Size(105, 22);
            this.tsmViewSortByBrand.Text = "Brand";
            this.tsmViewSortByBrand.Click += new System.EventHandler(this.tsmViewSortByBrand_Click);
            // 
            // tsmViewSortByYear
            // 
            this.tsmViewSortByYear.Name = "tsmViewSortByYear";
            this.tsmViewSortByYear.Size = new System.Drawing.Size(105, 22);
            this.tsmViewSortByYear.Text = "Year";
            this.tsmViewSortByYear.Click += new System.EventHandler(this.tsmViewSortByYear_Click);
            // 
            // tsmViewSortByPrice
            // 
            this.tsmViewSortByPrice.Name = "tsmViewSortByPrice";
            this.tsmViewSortByPrice.Size = new System.Drawing.Size(105, 22);
            this.tsmViewSortByPrice.Text = "Price";
            this.tsmViewSortByPrice.Click += new System.EventHandler(this.tsmViewSortByPrice_Click);
            // 
            // tsmViewSelect
            // 
            this.tsmViewSelect.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmViewSelectBrand,
            this.tsmViewSelectYear});
            this.tsmViewSelect.Name = "tsmViewSelect";
            this.tsmViewSelect.Size = new System.Drawing.Size(111, 22);
            this.tsmViewSelect.Text = "Select";
            // 
            // tsmViewSelectBrand
            // 
            this.tsmViewSelectBrand.Name = "tsmViewSelectBrand";
            this.tsmViewSelectBrand.Size = new System.Drawing.Size(105, 22);
            this.tsmViewSelectBrand.Text = "Brand";
            this.tsmViewSelectBrand.Click += new System.EventHandler(this.tsmViewSelectBrand_Click);
            // 
            // tsmViewSelectYear
            // 
            this.tsmViewSelectYear.Name = "tsmViewSelectYear";
            this.tsmViewSelectYear.Size = new System.Drawing.Size(105, 22);
            this.tsmViewSelectYear.Text = "Year";
            this.tsmViewSelectYear.Click += new System.EventHandler(this.tsmViewSelectYear_Click);
            // 
            // tsmHelp
            // 
            this.tsmHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmHelpHowToUse,
            this.tsmHelpAbout});
            this.tsmHelp.Name = "tsmHelp";
            this.tsmHelp.Size = new System.Drawing.Size(44, 20);
            this.tsmHelp.Text = "Help";
            // 
            // tsmHelpHowToUse
            // 
            this.tsmHelpHowToUse.Name = "tsmHelpHowToUse";
            this.tsmHelpHowToUse.Size = new System.Drawing.Size(137, 22);
            this.tsmHelpHowToUse.Text = "How To Use";
            // 
            // tsmHelpAbout
            // 
            this.tsmHelpAbout.Name = "tsmHelpAbout";
            this.tsmHelpAbout.Size = new System.Drawing.Size(137, 22);
            this.tsmHelpAbout.Text = "About";
            this.tsmHelpAbout.Click += new System.EventHandler(this.tsmHelpAbout_Click);
            // 
            // pbImageOfCar
            // 
            this.pbImageOfCar.Image = global::CarManager.Properties.Resources.placeholder;
            this.pbImageOfCar.InitialImage = global::CarManager.Properties.Resources.placeholder;
            this.pbImageOfCar.Location = new System.Drawing.Point(301, 299);
            this.pbImageOfCar.Name = "pbImageOfCar";
            this.pbImageOfCar.Size = new System.Drawing.Size(266, 233);
            this.pbImageOfCar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImageOfCar.TabIndex = 10;
            this.pbImageOfCar.TabStop = false;
            this.pbImageOfCar.Click += new System.EventHandler(this.pbImageOfCar_Click);
            // 
            // listInfomation
            // 
            this.listInfomation.Enabled = false;
            this.listInfomation.FormattingEnabled = true;
            this.listInfomation.Location = new System.Drawing.Point(6, 19);
            this.listInfomation.Name = "listInfomation";
            this.listInfomation.Size = new System.Drawing.Size(254, 173);
            this.listInfomation.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 544);
            this.Controls.Add(this.pbImageOfCar);
            this.Controls.Add(this.btnNextCar);
            this.Controls.Add(this.btnPreviousCar);
            this.Controls.Add(this.lbLastEdited);
            this.Controls.Add(this.btnSaveCar);
            this.Controls.Add(this.btnEditCar);
            this.Controls.Add(this.lbCreated);
            this.Controls.Add(this.grpInformation);
            this.Controls.Add(this.grpGearbox);
            this.Controls.Add(this.grpBodyType);
            this.Controls.Add(this.grpCharacteristics);
            this.Controls.Add(this.msMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.msMain;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Car Manager";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.grpCharacteristics.ResumeLayout(false);
            this.grpCharacteristics.PerformLayout();
            this.grpBodyType.ResumeLayout(false);
            this.grpBodyType.PerformLayout();
            this.grpGearbox.ResumeLayout(false);
            this.grpGearbox.PerformLayout();
            this.grpInformation.ResumeLayout(false);
            this.grpInformation.PerformLayout();
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImageOfCar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCharacteristics;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtMilage;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.Label lbMilage;
        private System.Windows.Forms.Label lbYear;
        private System.Windows.Forms.Label lbModel;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.GroupBox grpBodyType;
        private System.Windows.Forms.RadioButton rbCoupe;
        private System.Windows.Forms.RadioButton rbSUV;
        private System.Windows.Forms.RadioButton rbConvertible;
        private System.Windows.Forms.RadioButton rbMPV;
        private System.Windows.Forms.RadioButton rbEstate;
        private System.Windows.Forms.RadioButton rbSaloon;
        private System.Windows.Forms.RadioButton rbHatchback;
        private System.Windows.Forms.GroupBox grpGearbox;
        private System.Windows.Forms.RadioButton rbAutomatic;
        private System.Windows.Forms.RadioButton rbManual;
        private System.Windows.Forms.GroupBox grpInformation;
        private System.Windows.Forms.TextBox txtAdd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClearInformation;
        private System.Windows.Forms.Label lbCreated;
        private System.Windows.Forms.Button btnEditCar;
        private System.Windows.Forms.Button btnSaveCar;
        private System.Windows.Forms.Label lbLastEdited;
        private System.Windows.Forms.Button btnPreviousCar;
        private System.Windows.Forms.Button btnNextCar;
        private System.Windows.Forms.PictureBox pbImageOfCar;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmFile;
        private System.Windows.Forms.ToolStripMenuItem tsmFileOpen;
        private System.Windows.Forms.ToolStripMenuItem tsmFileSave;
        private System.Windows.Forms.ToolStripMenuItem tsmFileSaveAs;
        private System.Windows.Forms.ToolStripMenuItem tsmExit;
        private System.Windows.Forms.ToolStripMenuItem tsmEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmEditCut;
        private System.Windows.Forms.ToolStripMenuItem tsmEditCopy;
        private System.Windows.Forms.ToolStripMenuItem tsmEditPaste;
        private System.Windows.Forms.ToolStripMenuItem tsmEditDelete;
        private System.Windows.Forms.ToolStripMenuItem tsmView;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSortBy;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSortByBrand;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSortByYear;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSortByPrice;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSelect;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSelectBrand;
        private System.Windows.Forms.ToolStripMenuItem tsmViewSelectYear;
        private System.Windows.Forms.ToolStripMenuItem tsmHelp;
        private System.Windows.Forms.ToolStripMenuItem tsmHelpHowToUse;
        private System.Windows.Forms.ToolStripMenuItem tsmHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem tsmFileNew;
        private System.Windows.Forms.ToolStripMenuItem tsmFileNewCar;
        private System.Windows.Forms.ToolStripMenuItem tsmFileNewDatabase;
        private System.Windows.Forms.ListBox listInfomation;
    }
}

