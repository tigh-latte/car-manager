﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarManager {
    public partial class SelectBrandForm : Form {
        public List<string> Results;

        public SelectBrandForm(List<Car> cars) {
            InitializeComponent();
            // Initialises List<> to hold Brand's from passed List<>. Brand only added if it has not already been added before.
            Results = new List<string>();
            foreach (Car c in cars) {
                if (!clbBrands.Items.Contains(c.Brand)) clbBrands.Items.Add(c.Brand);
            }
        }

        // Handles btnOk's click event.
        private void btnOk_Click(object sender, EventArgs e) {
            // If no item is checked, return.
            if (clbBrands.CheckedItems.Count == 0) return;

            // Iterate through the checkbox and add every checked selection to Results.
            for (int i = 0; i < clbBrands.Items.Count; i++) {
                if (clbBrands.GetItemChecked(i)) Results.Add(clbBrands.Items[i].ToString());
            }

            // Set the DialogResult to OK so MainForm knows to process input.
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
