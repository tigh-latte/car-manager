﻿using System;

public class Edit
{
    // Used as a buffer to hold a copy of a user sepecfied car.
    static Car car;

    // Copies reference to instance of passed Car object.
    public static void Copy(Car c) {
        car = c;
    }

    // Returns reference of instance of copied Car object.
    public static Car Paste() {
        return car;
    }
}
