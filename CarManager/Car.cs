﻿using CarManager.Properties;
using System;
using System.Collections.Generic;
using System.IO;

public class Car {
    // Default constructor.
    public Car() {
        Information = new List<string>();
        dateCreated = DateTime.Now;
    }

    // Constructor to create a car from a single, correctly formatted, string.
    public Car(string lineFromFile) {
        // If the file doesn't contain the set symbol, then throw an exception that will later be caught to display
        // a relevant error message.
        if (!lineFromFile.Contains("¦")) throw new IOException();

        // Split the loaded information and assign it to the corresponding member variables.
        string[] carInfo = lineFromFile.Split('¦');
        brand = carInfo[0];
        model = carInfo[1];
        year = Convert.ToInt32(carInfo[2]);
        milage = Convert.ToInt32(carInfo[3]);
        price = Convert.ToDecimal(carInfo[4]);

        bodyType = carInfo[5];
        gearbox = carInfo[6];

        dateCreated = DateTime.Parse(carInfo[7]);
        dateLastEdited = DateTime.Parse(carInfo[8]);

        photograph = carInfo[9];

        information = new List<string>();
        carInfo = carInfo[10].Split('~');
        foreach (string s in carInfo) {
            if(s.Trim().Length > 0) information.Add(s);
        }


    }

    public override string ToString() {
        // Save all the car's members to a single string.
        // Used ¦ as the separation character instead of # because since we're dealing with cars that have model numbers,
        // it is likely that an end user could use # in a field to prefix a model number
        string str = brand + "¦" + model + "¦" + year + "¦" + milage + "¦" + price + "¦" + bodyType + "¦" + gearbox + "¦" +
            dateCreated.ToString() + "¦" + dateLastEdited.ToString() + "¦" + photograph + "¦";

        // Use a different seperation charactor for the list box as we don't know for certain how many items will be added to it.
        foreach (string s in information) str += s + "~";

        return str[str.Length - 1] == '~' ? str.Remove(str.Length - 1) : str;
    }

    // Member variables.
    private string brand;
    private string model;
    private int year;
    private int milage;
    private decimal price;

    private string bodyType;
    private string gearbox;

    private List<string> information;
    private string photograph;

    private DateTime dateCreated;
    private DateTime dateLastEdited;


    // Getters and setters.
    public string Brand {
        get {
            return brand;
        }

        set {
            // Formats the brand name so that only the first letter is upper case.
            brand = value[0].ToString().ToUpper() + value.Substring(1).ToLower();
        }
    }

    public string Model {
        get {
            return model;
        }

        set {
            // Formats the model name so that only the first letter of every word is upper case.
            string[] split = value.Split(' ');
            string fullModel = "";
            for (int i = 0; i < split.Length; i++) {
                split[i] = split[i][0].ToString().ToUpper() + split[i].Substring(1).ToLower() + " ";
                fullModel += split[i];
            }
            model = fullModel.Trim();
        }
    }

    public int Year {
        get {
            return year;
        }

        set {
            year = value;
        }
    }

    public int Milage {
        get {
            return milage;
        }

        set {
            milage = value;
        }
    }

    public decimal Price {
        get {
            return price;
        }

        set {
            price = value;
        }
    }

    public string BodyType {
        get {
            return bodyType;
        }

        set {
            bodyType = value;
        }
    }

    public string Gearbox {
        get {
            return gearbox;
        }

        set {
            gearbox = value;
        }
    }

    public List<string> Information {
        get {
            return information;
        }

        set {
            information = value;
        }
    }

    public DateTime DateCreated {
        get {
            return dateCreated;
        }

        set {
            dateCreated = value;
        }
    }

    public string Photograph {
        get {
            return photograph;
        }

        set {
            photograph = value;
        }
    }

    public DateTime DateLastEdited {
        get {
            return dateLastEdited;
        }

        set {
            dateLastEdited = value;
        }
    }
}
