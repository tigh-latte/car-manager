﻿using CarManager.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarManager {
    public partial class MainForm : Form {
        // Member variables.
        List<Car> cars;                                     // List<> to hold all the currently loaded cars.
        Car car;                                            // Car to hold the currently displayed car.
        private string pathAndFileName;                     // string to hold the file name of the currently opened database.

        public MainForm() {
            InitializeComponent();
            // Initialise relevant variables.
            cars = new List<Car>();
            car = new Car();
            pathAndFileName = "";
            lbCreated.Text = "Created: " + car.DateCreated.ToString("MMMM dd, yyyy h:mmtt");

            KeyPreview = true;          // Allows keyboard shortcuts to be handles, further down the form.
        }

        // Convenience method for easily updating every component of the form.
        private void updateDisplay() {
            txtBrand.Text = car.Brand;
            txtModel.Text = car.Model;
            txtYear.Text = (car.Year == 0 ? "" : car.Year.ToString());
            txtMilage.Text = (car.Milage == 0 ? "" : car.Milage.ToString());
            txtPrice.Text = (car.Price == 0 ? "" : Math.Round(car.Price, 2).ToString());

            // Loops through every RadioButton in grpBodyType.
            // OfType<> ensures only the passed Object is selected, as to avoid InvalidCastException's
            foreach (RadioButton rb in grpBodyType.Controls.OfType<RadioButton>()) {
                rb.Checked = false;                                 // Incase no body type is selected.
                if (car.BodyType == rb.Name) rb.Checked = true;
            }

            // Loops through every RadioButton in grpGearbox.
            // OfType<> ensures only the passed Object is selected, as to avoid InvalidCastException's.
            foreach (RadioButton rb in grpGearbox.Controls.OfType<RadioButton>()) {
                rb.Checked = false;                                 // Incase no gearbox is selected.
                if (car.Gearbox == rb.Name) rb.Checked = true;
            }

            // If no image was loaded, set the default image.
            pbImageOfCar.ImageLocation = car.Photograph;
            if (pbImageOfCar.Image == null) pbImageOfCar.Image = pbImageOfCar.InitialImage;

            listInfomation.Items.Clear();
            foreach (string s in car.Information) listInfomation.Items.Add(s);
            txtAdd.Clear();

            // Displays the DateTime object's information in a custom format.
            lbCreated.Text = "Created: " + car.DateCreated.ToString("MMMM dd, yyyy h:mmtt");
            lbLastEdited.Text = "Last Edit: " + car.DateLastEdited.ToString("MMMM dd, yyyy h:mmtt");

            // Displays information about the next car and previous car in btnNextCar and btnPreviousCar,
            // if there are 2 or more Car objects in the List<> cars.
            if (cars.Count > 1) {
                Car prevCar, nextCar;
                // If there are two cars in the database and the user is adding a third, not just browsing them.
                if (cars.Count == 2 && !cars.Contains(car)) {
                    prevCar = cars[0];
                    nextCar = cars[1];
                } else {
                    // Logical lines to allow circular selection in both directions.
                    prevCar = cars[(cars.IndexOf(car) + (cars.Count - 1)) % cars.Count];
                    nextCar = cars[(cars.IndexOf(car) + 1) % cars.Count];
                }

                btnPreviousCar.Text = "<< Previous Car\n" +
                    prevCar.Brand + " " + prevCar.Model;

                btnNextCar.Text = "Next Car >>\n" +
                    nextCar.Brand + " " + nextCar.Model;
            } else {
                btnNextCar.Text = "Next Car >>";
                btnPreviousCar.Text = "<< Previous Car";
            }
        }

        // Adds the content of txtAdd to listInformation.
        private void addItemToInformationBox() {
            // If there is text in the txtAdd.
            if (txtAdd.Text.Trim().Length > 0) {
                listInfomation.Items.Add(txtAdd.Text.Trim());
            }

            txtAdd.Clear();
            txtAdd.Focus();
        }

        // Either allows or disallows editing depending on the value of the bool passed.
        private void allowEdits(bool editable) {
            foreach (TextBox tb in grpCharacteristics.Controls.OfType<TextBox>()) tb.ReadOnly = !editable;
            grpBodyType.Enabled = editable;
            grpGearbox.Enabled = editable;
            btnClearInformation.Enabled = editable;
            btnAdd.Enabled = editable;
            txtAdd.ReadOnly = !editable;
            pbImageOfCar.Enabled = editable;
        }

        // Creates a new car, unlocks the form for editing.
        private void createNewCar() {
            car = new Car();
            allowEdits(true);
            txtBrand.Focus();
            updateDisplay();
        }

        // Deletes the current car from the database.
        private void deleteCar() {
            // If there are no cars in the database, do nothing.
            if (cars.Count == 0) {
                MessageBox.Show("Nothing to delete!", "Deletion Error.");
                return;
            }
            // If the current car is not saved.
            else if(!cars.Contains(car)) {
                car = cars[cars.Count - 1];
                updateDisplay();
            }
            // If there is only one car.
            else if (cars.Count == 1) {
                // Remove the current car and enable editing.
                cars.Remove(car);
                car = new Car();
                allowEdits(true);
                updateDisplay();
            } else {
                // Remove the current car, disable editing and page to the previous car.
                int index = cars.IndexOf(car);
                cars.Remove(car);
                // Ternary operator. If the deleted car's index was 0, load the current car at index,
                // otherwise load the previous index.
                car = cars[index == 0 ? 0 : index - 1];
                allowEdits(false);
                updateDisplay();
            }
        }

        // Selects the next car in the List<>, and locks the form.
        private void nextCar() {
            // If the List<> is not empty.
            if (cars.Count > 0) {
                car = cars[(cars.IndexOf(car) + 1) % cars.Count];           // Logical line to allow for rightward circular selection.
                allowEdits(false);
                updateDisplay();
            }
        }

        // Selects the previous car in the List<>, and locks the form.
        private void previousCar() {
            // If the List<> is not empty.
            if (cars.Count > 0) {
                int length = cars.Count;
                car = cars[(cars.IndexOf(car) + (length - 1)) % length];    // Logical line to allow for leftward cirucular selection.
                allowEdits(false);
                updateDisplay();
            }
        }

        // Saves the information to the Car object and locks the form.
        private bool saveCar() {
            // Ensures the required fields are entered.
            if (txtBrand.Text.Trim().Length == 0 || txtModel.Text.Trim().Length == 0) {
                MessageBox.Show("The Brand and Model fields are required!", "Completion Error");
                return false;
            }

            /* Trim() removes all whitespace from the start and the end of a string. */

            car.Brand = txtBrand.Text.Trim();
            car.Model = txtModel.Text.Trim();
            // If txtYear.Text is not a valid integer or a sensical year, display and error message.
            try {
                if (txtYear.Text.Trim().Length > 0) car.Year = Convert.ToInt32(txtYear.Text.Trim());
                if (car.Year < 0 || car.Year > DateTime.Now.Year) { car.Year = 0; throw new FormatException(); }
            } catch (FormatException) {
                MessageBox.Show("Invalid year entered, value has not been saved.", "Invalid Year.");
                txtYear.Clear();
            }

            // If txtMilage.Text is not a valid integer or a sensical figure, display an error message.
            try {
                if (txtMilage.Text.Trim().Length > 0) car.Milage = Convert.ToInt32(txtMilage.Text.Trim());
                if (car.Milage < 0) { car.Milage = 0; throw new FormatException(); }
            } catch (FormatException) {
                MessageBox.Show("Milage is not a positive whole number, value has not been saved.", "Invalid Milage.");
                txtMilage.Clear();
            }

            // If txtPrice.Text is not a valid double or a sensical figure, display an error message.
            try {
                if (txtPrice.Text.Trim().Length > 0) car.Price = Convert.ToDecimal(txtPrice.Text.Trim());
                if (car.Price < 0) { car.Price = 0; throw new FormatException(); }
            } catch (FormatException) {
                MessageBox.Show("Price is not valid currency, value has not been saved.", "Invalid Price");
                txtPrice.Clear();
            }

            // Loops through every RadioButton in grpBodyType.
            // OfType<> ensures only the passed Object is selected, as to avoid InvalidCastException's.
            foreach (RadioButton rb in grpBodyType.Controls.OfType<RadioButton>())
                if (rb.Checked) car.BodyType = rb.Name;

            // Loops through every RadioButton in grpGearbox.
            // OfType<> ensures only the passed Object is selected, as to avoid InvalidCastException's.
            foreach (RadioButton rb in grpGearbox.Controls.OfType<RadioButton>())
                if (rb.Checked) car.Gearbox = rb.Name;

            car.Photograph = pbImageOfCar.ImageLocation;

            car.Information.Clear();
            foreach (string s in listInfomation.Items)
                car.Information.Add(s);

            // Updates Car.dateLastEdited to the current date and time.
            car.DateLastEdited = DateTime.Now;

            // If car is not in the List<>, add it.
            if (!cars.Contains(car)) cars.Add(car);

            // Lock form
            allowEdits(false);
            updateDisplay();

            return true;
        }

        // Displays a dialog for selecting an Image.
        private void showImagePickerDialog() {
            OpenFileDialog imagePickerDialog = new OpenFileDialog();
            imagePickerDialog.Title = "Select Picture";

            // Applies filter for defined file types.
            imagePickerDialog.Filter = "Image Files (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png| All Files (*.*) | *.*";
            imagePickerDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures).ToString();

            // If the OK button was clicked.
            if (imagePickerDialog.ShowDialog() == DialogResult.OK) {
                pbImageOfCar.ImageLocation = imagePickerDialog.FileName;
            }
        }

        // Handles btnSaveCar's click event.
        private void btnSaveCar_Click(object sender, EventArgs e) {
            saveCar();
        }

        // Handles btnEditCar's click event.
        private void btnEditCar_Click(object sender, EventArgs e) {
            allowEdits(true);
        }

        // Handles btnAdd's click event.
        private void btnAdd_Click(object sender, EventArgs e) {
            addItemToInformationBox();
        }

        // Handles txtAdd's key down event. 
        private void txtAdd_KeyDown(object sender, KeyEventArgs e) {
            // If the enter key was pressed.
            if (e.KeyCode == Keys.Enter) {
                addItemToInformationBox();

                // Ensures no further processing of the key press is carried out.
                e.Handled = true; e.SuppressKeyPress = true;
            }
        }

        // Handles btnClearInformation's click event.
        private void btnClearInformation_Click(object sender, EventArgs e) {
            car.Information.Clear();
            listInfomation.Items.Clear();
        }

        // Handles btnPreviousCar's click event.
        private void btnPreviousCar_Click(object sender, EventArgs e) {
            previousCar();
        }

        // Handles btnNextCar's click event.
        private void btnNextCar_Click(object sender, EventArgs e) {
            nextCar();
        }

        // Handles pbImageOfCar's click event.
        private void pbImageOfCar_Click(object sender, EventArgs e) {
            showImagePickerDialog();
        }

        // Handles tsmExit's click event.
        private void tsmExit_Click(object sender, EventArgs e) {
            Close();
        }

        // Handles tsmEditDelete's click event.
        private void tsmEditDelete_Click(object sender, EventArgs e) {
            deleteCar();
        }

        // Handles tsmViewSortByYear's click event.
        private void tsmViewSortByYear_Click(object sender, EventArgs e) {
            // Line of logic that orders a list of objects by a property of the object. It operates as follows:
            // Object.OrderBy(Object => Object.Property).ToList();
            cars = cars.OrderBy(c => c.Year).ToList();
            car = cars[0];              // Start at first car.
            updateDisplay();
        }

        // Handles tsmViewSortByPrice's click event.
        private void tsmViewSortByPrice_Click(object sender, EventArgs e) {
            // Line of logic that orders a list of objects by a property of the object. It operates as follows:
            // Object.OrderBy(Object => Object.Property).ToList();
            cars = cars.OrderBy(c => c.Price).ToList();
            car = cars[0];              // Start at first car.
            updateDisplay();
        }

        // Handles tsmViewSortByBand's click event.
        private void tsmViewSortByBrand_Click(object sender, EventArgs e) {
            // Line of logic that orders a list of objects by a property of the object. It operates as follows:
            // Object.OrderBy(Object => Object.Property).ToList();
            cars = cars.OrderBy(c => c.Brand).ToList();
            car = cars[0];              // Start at first car.
            updateDisplay();
        }

        // Handles tsmEditCopy's click event.
        private void tsmEditCopy_Click(object sender, EventArgs e) {
            // If the database contains the current car.
            if (cars.Contains(car)) {
                Edit.Copy(car);
                tsmEditPaste.Enabled = true;
            } else {
                MessageBox.Show("You can only copy saved cars, please save the car first.", "Car not saved to database.");
            }
        }

        // Handles tsmEditCut's click event.
        private void tsmEditCut_Click(object sender, EventArgs e) {
            // If the database contains the current car.
            if (cars.Contains(car)) {
                Edit.Copy(car);
                deleteCar();
                tsmEditPaste.Enabled = true;
            } else {
                MessageBox.Show("You can only cut saved cars, please save the car first.", "Car not saved to database.");
            }
        }

        // Handles tsmEditPaste's click event.
        private void tsmEditPaste_Click(object sender, EventArgs e) {
            // Load Car from buffer.
            Car tempCar = Edit.Paste();

            // If the loaded Car is not a part of the database.
            if (!cars.Contains(tempCar)) {
                cars.Add(tempCar);
                car = tempCar;
                allowEdits(false);
                updateDisplay();
            } else {
                MessageBox.Show(tempCar.Brand + " " + tempCar.Model + " already in database!", "Pasting Error");
            }
        }

        // Handles tsmFileNewCar's click event.
        private void tsmFileNewCar_Click(object sender, EventArgs e) {
            createNewCar();
        }

        // Handles tsmFileNewDatabase's click event.
        private void tsmFileNewDatabase_Click(object sender, EventArgs e) {
            // Empty the List<>, create a new Car and reset the file name.
            cars.Clear();
            createNewCar();
            pathAndFileName = "";

            tsmFileSave.Enabled = false;
        }

        // Handles tsmFileSave's click event.
        private void tsmFileSave_Click(object sender, EventArgs e) {
            // If the database is empty, display and error and return.
            if (cars.Count == 0) {
                MessageBox.Show("Nothing to save.", "Empty Database");
                return;
            }

            // If the current car is not a part of the database, offer to save it.
            if (!cars.Contains(car)) {
                // If the user confirms the offer to save.
                if (MessageBox.Show("Current car isn\'t saved to database, do you want to save it?", "Save Current Car?", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    // If the save was not successful, display an error but continue with the save.
                    if (!saveCar()) MessageBox.Show("Current car not saved due to previously displayed issues.", "Car Not Saved.");
                }
            }

            StreamWriter writer = null;
            //Stream textOut = null;
            try {
                writer = new StreamWriter(File.Create(pathAndFileName));
                foreach (Car c in cars) writer.WriteLine(c.ToString());

            } catch (IOException) {
                // If save was unsuccessful, display an error.
                MessageBox.Show("Error saving the file, please try again.", "IO Error.");
            } finally {
                if (writer != null) writer.Close();
            }

            allowEdits(false);
        }

        private void tsmFileSaveAs_Click(object sender, EventArgs e) {
            // Creates new instance of SaveFileDialog and assigns it's traits.
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();     // Default director is My Documents.
            sfd.RestoreDirectory = true;
            sfd.CreatePrompt = false;
            sfd.OverwritePrompt = true;
            // If the file name is empty, create one.
            if (pathAndFileName.Equals("")) sfd.FileName = cars.Count > 0 ? cars.Count + " car database" : "Empty Database";
            else {
                // Extracts the path from pathAndFileName.
                sfd.InitialDirectory = Path.GetDirectoryName(pathAndFileName);
                // Extracts the file name from pathAndFileName.
                sfd.FileName = Path.GetFileName(pathAndFileName);
            }
            sfd.AddExtension = true;
            sfd.DefaultExt = "txt";
            sfd.Filter = "Text Files (*.txt) | *.txt| All Files (*.*) | *.*";

            // If the database is empty, display and error and return.
            if (cars.Count == 0) {
                MessageBox.Show("Nothing to save.", "Empty Database");
                return;
            }

            // If the current car is not a part of the database, offer to save it.
            if (!cars.Contains(car)) {
                // If the user confirms the offer to save.
                if (MessageBox.Show("Current car isn\'t saved to database, do you want to save it?", "Save Current Car?", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    // If the save was not successful, display an error but continue with the save.
                    if (!saveCar()) MessageBox.Show("Current car not saved due to previously displayed issues.", "Car Not Saved.");
                }
            }

            // If the SaveFileDialog's OK button was clicked.
            if (sfd.ShowDialog() == DialogResult.OK) {
                // Set up IO components.
                StreamWriter writer = null;

                try {
                    writer = new StreamWriter(sfd.OpenFile());

                    // Iterate through every car in the List<> and write the car to a .txt file in a formatted string.
                    foreach (Car c in cars) writer.WriteLine(c.ToString());

                    pathAndFileName = sfd.FileName;
                } catch (IOException) {
                    // If save was unsuccessful, display an error.
                    MessageBox.Show("Error saving the file, please try again.", "IO Error.");
                } finally {
                    if (writer != null) writer.Close();
                    tsmFileSave.Enabled = true;
                }
            }

            allowEdits(false);
        }

        // Handles tsmFileOpen's click event.
        private void tsmFileOpen_Click(object sender, EventArgs e) {
            // Creates a new instance of OpenFileDialog and assigns its traits.
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "";
            ofd.Filter = "Text Files (*.txt) | *.txt| All Files (*.*) | *.*";
            ofd.DefaultExt = "txt";

            // If OpenFileDialog's OK button was clicked.
            if (ofd.ShowDialog() == DialogResult.OK) {
                // Reset the list of cars.
                cars = new List<Car>();

                // Setup IO components.
                StreamReader reader = null;

                try {
                    //Stream textIn = ofd.OpenFile();
                    reader = new StreamReader(ofd.OpenFile());

                    // While not at the end of the file, instantiate a Car object from the loaded line, and add it to the List<>.
                    while (reader.Peek() != -1) cars.Add(new Car(reader.ReadLine()));

                    pathAndFileName = ofd.FileName;
                    car = cars[0];
                    updateDisplay();
                    allowEdits(false);
                } catch (IOException) {
                    MessageBox.Show(ofd.SafeFileName + " is not correctly formatted!", "Loading Error");
                    pathAndFileName = "";
                    updateDisplay();
                    allowEdits(true);
                    tsmFileSave.Enabled = false;
                } finally {
                    if (reader != null) reader.Close();
                    tsmFileSave.Enabled = true;
                }
            }
        }

        // Handles tsmViewSelectBrand's click event.
        private void tsmViewSelectBrand_Click(object sender, EventArgs e) {
            // If the database is empty, return.
            if (cars.Count == 0) return;
            // Creates new instance of SelectBrandForm.
            SelectBrandForm selectBrand = new SelectBrandForm(cars);

            // If SelectBrandForm's OK button was clicked.
            if (selectBrand.ShowDialog() == DialogResult.OK) {
                // Iterate through the List<> backwards to prevent errors.
                for (int i = cars.Count - 1; i >= 0; i--) {
                    // If the car's brand was not selected, delete it.
                    bool selected = false;
                    foreach (string s in selectBrand.Results) {
                        if (cars[i].Brand.Equals(s)) selected = true;
                    }
                    if (!selected) cars.Remove(cars[i]);
                }

                car = cars[0];
                allowEdits(false);
                updateDisplay();
            }
        }

        // Handles tsmViewSelectYear's click event.
        private void tsmViewSelectYear_Click(object sender, EventArgs e) {
            // If the List<> is empty, return.
            if (cars.Count == 0) return;
            // Creates a new instance of SelectYearForm.
            SelectYearForm selectYear = new SelectYearForm(cars);

            // If SelectYearFrom's OK button was clicked.
            if(selectYear.ShowDialog() == DialogResult.OK) {
                // Iterate through the List<> backwards to prevent the errors.
                for(int i = cars.Count - 1; i >= 0; i--) {
                    // If the current car's year is not within the specified range, remove it.
                    if (cars[i].Year < selectYear.yearFrom || cars[i].Year > selectYear.yearTo) cars.Remove(cars[i]);
                }
                car = cars[0];
                allowEdits(false);
                updateDisplay();
            }
        }

        // Handles tsmHelpAbout's click event.
        private void tsmHelpAbout_Click(object sender, EventArgs e) {
            new AboutForm().ShowDialog();
        }

        // Handles MainForm's key press events.
        private void MainForm_KeyDown(object sender, KeyEventArgs e) {
            // If the pressed key is Ctrl.
            if (e.Control) {
                // If the pressed key is E, allow edits.
                if (e.KeyCode == Keys.E) {
                    allowEdits(true);
                    btnEditCar.Focus();
                    e.Handled = true; e.SuppressKeyPress = true;
                    // If the pressed key is S, save the current car.
                } else if (e.KeyCode == Keys.S) {
                    if (saveCar()) btnSaveCar.Focus();
                    e.Handled = true; e.SuppressKeyPress = true;
                    // If the pressed key is D, delete the current car.
                } else if (e.KeyCode == Keys.D) {
                    if (cars.Count > 0) deleteCar();
                    else MessageBox.Show("Nothing to delete!", "Deletion Error.");
                    e.Handled = true; e.SuppressKeyPress = true;
                    // If the pressed key is P, allow the user to select a new image.
                } else if (e.KeyCode == Keys.P) {
                    showImagePickerDialog();
                    e.Handled = true; e.SuppressKeyPress = true;
                    // If the pressed key is Right Arrow, move onto the next car.
                } else if (e.KeyCode == Keys.Right) {
                    nextCar();
                    btnNextCar.Focus();
                    e.Handled = true; e.SuppressKeyPress = true;
                    // If the pressed key is Left Arrow, move onto the previous car.
                } else if (e.KeyCode == Keys.Left) {
                    previousCar();
                    btnPreviousCar.Focus();
                    e.Handled = true; e.SuppressKeyPress = true;
                }
            }
        }
    } /// End MainForm
} // End CarManager
